FROM ghcr.io/facthunder/cppcheck:latest

COPY dist/cppcheck_codequality-1.3.1.post10-py3-none-any.whl /tmp/cppcheck_codequality-1.3.1.post10-py3-none-any.whl
RUN pip install /tmp/cppcheck_codequality-1.3.1.post10-py3-none-any.whl

LABEL maintainer="poffey21"
WORKDIR /src
