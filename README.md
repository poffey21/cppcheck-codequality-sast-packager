# cppcheck-codequality-sast-packager

Relies upon [Facthunder's cppcheck container](https://github.com/Facthunder/cppcheck)

## Getting started

This allows a user to 

```yaml
gitlab_sast_artifact_report_using_container:
  image: registry.gitlab.com/poffey21/cppcheck-codequality-sast-packager/main:latest
  stage: test
  script:
    - cppcheck --xml --enable=warning,style,performance ./src 2> cppcheck.xml
    - cppcheck-codequality -o sast.json
  artifacts:
    when: always
    paths:
      - sast.json
    reports:
      sast: sast.json
```


